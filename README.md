# freeradius

## Configuració bàsica de servidor RADIUS amb FreeRADIUS (directament)

#### 1.Crea un equip virtual amb Ubuntu Server i instal·la i configura el servidor RADIUS FreeRADIUS.

INstalem FreeRADIUS

![](assets/README-6808dc0d.png)

Trobem el fitxer de configuracio de clients

![](assets/README-13da28a5.png)

Al final del seguent fitxer afegim les següents linees

![](assets/README-b402a561.png)

![](assets/README-864851fb.png)

Afegim les seguents linees a partir del fitxer que s'indica a continuació(s'afegeix al final del tot)

![](assets/README-02f359a5.png)

![](assets/README-79060b50.png)

Iniciem el servei i ens loguejem

![](assets/README-4f234255.png)


#### 2.Fes la prova que el servidor està escoltant des d’un client Ubuntu (connectat a la mateixa xarxa) amb la comanda “radtest”.
#### 3.Connecta un client al servidor RADIUS per comprovar que valida les credencials. Recorda que aquest client és normalment un NAS o Network Access Server que podria ser un ount d'accés Wifi, un sevidor VPN, etc.

![](assets/README-f538b877.png)

#### 4.Configura l’equip per a poder atendre les peticions d’un punt d’accés que farà de NAS.

Primer descarreguem el repositori de la pàgina original

![](assets/README-594b8424.png)

En ubuntu server modifiquem un client de la següent manera

![](assets/README-54d884f4.png)

entrem en el navegador del nostre client amb ipclient:8020 ens loguejem i afemig el servidor radius

![](assets/README-abb48241.png)

Comprovem que la conexio es realitza correctament on posa status:good

![](assets/README-22304d85.png)

## Configuració de servidor RADIUS amb PFSense

#### 1.Crea un equip virtual i instal·la PFSense i el mòdul FreeRADIUS. Vigileu, per que el PFSense per defecte només es pot administrar des de la interfície LAN (bloca els accessos al gestor Web i ssh que arribin per la interfície WAN).

![](assets/README-487a6fda.png)

#### 2.Prova des del propi Pfsense que funciona el servidor RADIUS.

Configurem les ips i els ports per els que ens podrem loguejar

![](assets/README-c8a66481.png)

Entrem a system > user manager > authenticationserver i l'omplim de la següent manera

![](assets/README-cbfcaa48.png)

![](assets/README-de90062b.png)

![](assets/README-4cfc5ec3.png)

Comprovem que podem loguejarnos

![](assets/README-a1fb6b50.png)

#### 3.Fes la prova que el servidor està escoltant des d’un client Ubuntu (connectat a la xarxa LAN del PfSense) amb la comanda “radtest”.
